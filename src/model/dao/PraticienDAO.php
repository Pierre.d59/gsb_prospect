<?php
/**
 * File :        PraticienDAO.php
 * Location :    gsb_prospects/src/model/dao/PraticienDAO.php
 * PHP Version : 7.0
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\model\dao;

use \PDO;
use \PDOException;
use gsb_prospects\kernel\NotImplementedException;
use gsb_prospects\model\objects\Praticien;

/**
 * Class PraticienDAO
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
final class PraticienDAO extends AbstractDAO implements IDAO
{
    protected $table = "praticien";
    protected $class = "gsb_prospects\model\objects\Praticien";
    protected $fields = [ 
        "id", "nom", "prenom", "adresse", "id_Ville", "id_Type_Praticien" 
    ];
public function addPraticien($nom, $prenom, $adresse, $id_Ville, $id_Type_Praticien, $id_niveau_praticien)
    {
        // 1. Connexion
        $dbh = $this->getConnexion();

        // 2. Definition de la requ�te SQL
        $query = "
            INSERT INTO `praticien`
            (`nom`, `prenom`, `adresse`, `id_Ville`, `id_Type_Praticien`)
            VALUES
            (:nom, :prenom, :adresse, :id_Ville, :id_Type_Praticien);
        ";

        // 3. pr�paration de la requ�te
        $sth = $dbh->prepare($query);

        // 4. fourniture des param�tres
        $sth->bindParam(":nom", $nom, PDO::PARAM_STR);
        $sth->bindParam(":prenom", $prenom, PDO::PARAM_STR);
        $sth->bindParam(":adresse", $adresse, PDO::PARAM_STR);
        $sth->bindParam(":id_Ville", $id_Ville, PDO::PARAM_STR);
        $sth->bindParam(":id_Type_Praticien", $id_Type_Praticien, PDO::PARAM_STR);
		$sth->bindParam(":id_Type_Praticien", $id_niveau_praticien, PDO::PARAM_STR);

        // 5. ex�cution de la requ�te pr�par�e
        $res = $sth->execute();

        // 5.bis v�rification de l'ex�cution
        if (!$res)
        {
           throw new PDOException($sth->errorInfo()[2]);
        }
        else
        {
            // 6. r�cup�ration de l'id g�n�r�
            $id = $dbh->lastInsertId();
        }

        // 7. D�connexion
        $this->closeConnexion();

        return $id;
    }

    public function updatePraticien($id_praticien, $nom, $prenom, $adresse, $id_Ville, $id_Type_Praticien , $id_niveau_praticien)
    {
        // 1. Connexion
        $dbh = $this->getConnexion();

        // 2. Definition de la requ�te SQL
        $query = "UPDATE `praticien` SET `nom`=:nom, `prenom`=:prenom, `adresse`=:adresse, `id_Ville`=:id_Ville, `id_Type_Praticien`=:id_Type_Praticien WHERE `praticien`.`id` = :id";
    
        // 3. pr�paration de la requ�te
        $sth = $dbh->prepare($query);

        // 4. fourniture des param�tres
        $sth->bindParam(":id", $id_praticien, PDO::PARAM_STR);
        $sth->bindParam(":nom", $nom, PDO::PARAM_STR);
        $sth->bindParam(":prenom", $prenom, PDO::PARAM_STR);
        $sth->bindParam(":adresse", $adresse, PDO::PARAM_STR);
        $sth->bindParam(":id_Ville", $id_Ville, PDO::PARAM_STR);
        $sth->bindParam(":id_Type_Praticien", $id_Type_Praticien, PDO::PARAM_STR);
		$sth->bindParam(":id_Type_Praticien", $id_niveau_praticien, PDO::PARAM_STR);

        // 5. ex�cution de la requ�te pr�par�e
        $res = $sth->execute();

        // 5.bis v�rification de l'ex�cution
        if (!$res)
        {
           throw new PDOException($sth->errorInfo()[2]);
        }
        else
        {
            // 6. r�cup�ration de l'id g�n�r�
            $id = $dbh->lastInsertId();
        }

        // 7. D�connexion
        $this->closeConnexion();

        return $id;
    }

}
